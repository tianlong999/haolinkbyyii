<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'option-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">字段标有<span class="required">*</span> 是必须的.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'k'); ?>
		<?php echo $form->textField($model,'k',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'k'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'val'); ?>
		<?php echo $form->textField($model,'val',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'val'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->