<?php $this->renderPartial('/_include/header');?>
 
<style>
.panel{ max-width: 500px;margin: 0 auto;margin-top: 10px;}
.input-group{ max-width: 95%;margin: 0 auto;margin-top: 10px;}
.code{margin-left: 20px;margin-top: 10px;margin-bottom: 10px;}
</style>
<?php $form=$this->beginWidget('CActiveForm',array( 'enableAjaxValidation'=>false,
    'enableClientValidation'=>false));  ?>
<div class="panel">
            
                <div class='panel-heading'>后台登陆<span class="text-danger" style="margin-left: 20px;"><?php echo $form->error($model,'name'); ?><?php echo $form->error($model,'pwd'); ?><?php echo $form->error($model,'verify'); ?></span></div>
            <section id='input-groups' class="page-section">

                <div class="input-group">
                  <span class="input-group-addon"><i class='icon-user'></i></span>
                    <input type="text"  name="LoginForm[name]" class="form-control"  placeholder="请输入用户名">
                  <span class="glyphicon glyphicon-star"></span>
                </div>
                
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-key"></i></span>
                    <input type="password"  name="LoginForm[pwd]" class="form-control" placeholder="请输入密码">
                  <span class="glyphicon glyphicon-star"></span>
                </div>
                
                <div class="input-group">
                  <span class="input-group-addon"><i class="icon-chevron-down"></i></span>
                    <input type="text"  name="LoginForm[verify]" class="form-control" placeholder="请输入验证码">
                  <span class="glyphicon glyphicon-star"></span>
                </div>
                 <div class="code"> 
                     <?php $this->widget('CCaptcha',array('showRefreshButton'=>false,'clickableImage'=>true,'imageOptions'=>array('alt'=>'点击换图','title'=>'点击换图','style'=>'cursor:pointer'))); ?>
                </div>
            
                
                <div class="code">
                    <input type="submit" value='登陆' class="btn btn-primary" />
                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" value='清空' class="btn btn-warning" />
                </div>
            </section>
               
            </form>
        </div>

<?php $this->endWidget(); ?>
<?php $this->renderPartial('/_include/footer');?>