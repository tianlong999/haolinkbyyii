<?php

class HelpController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * 验证规则.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array(),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    /**
     * 清理缓存.
     * @return array access control rules
     */
    public function actionClear() {
        Yii::app()->cache->flush();
        XUtils::message('success', '缓存已经清理',  $this->createUrl('default/main'));
    }

}
