<?php

/**
 * This is the model class for table "{{content}}".
 *
 * The followings are the available columns in table '{{content}}':
 * @property integer $id
 * @property string $name
 * @property integer $is_hot
 * @property integer $cid
 * @property string $src
 */
class Content extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Content the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{content}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, cid, src', 'required'),
            array('is_hot, cid', 'numerical', 'integerOnly' => true),
            array('cid', 'check_cate'),
            array('name', 'length', 'max' => 30),
            array('src', 'length', 'max' => 200),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, is_hot, cid, src', 'safe', 'on' => 'search'),
        );
    }

    public function check_cate() {
       $row = $this->get_cid();
        if(!in_array($this->cid, $row)){
             $this->addError('cid', '没有此栏目');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cid' => array(self::BELONGS_TO, 'Category', 'id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'is_hot' => 'Is Hot',
            'cid' => 'Cid',
            'src' => 'Src',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('is_hot', $this->is_hot);
        $criteria->compare('cid', $this->cid);
        $criteria->compare('src', $this->src, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function get_all() {
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $row = $this->get_category();
        $sql = 'select a.name,a.is_hot,a.src,b.name as cname from {{content}} as a  INNER JOIN {{category}} as b
on a.cid=b.id where b.status=1;';
        $command->text = $sql;
        $arr = $column = $command->queryAll();
        $data = array();
        foreach ($arr as $k => $v) {
            if (in_array($v['cname'], $row)) {
                $data[$v['cname']][] = $v;
            }
        }
        return $data;
    }
    public function get_category(){
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $newSQL = 'select name as cname from {{category}}';
        $command->text = $newSQL;
        $row = $column = $command->queryColumn();
        return $row;
    }
    public function get_cid(){
        $connection = Yii::app()->db;
        $command = $connection->createCommand();
        $newSQL = 'select id from {{category}}';
        $command->text = $newSQL;
        $row = $column = $command->queryColumn();
        return $row;
    }

}
